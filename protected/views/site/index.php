<!--<h1>Semantik | Perpustakaan</h1>-->
<h1>?</h1>
<div class="row" style="color:blue;">
    Created By : Kabul Kurniawan (353925) | Nur Ichsan (372281) | Ryan Arief Misnadin (372059) | Udhi Permana (372301)
</div>
<div class="col-lg-12">
    <div class="row">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'contact-form',
            'enableClientValidation' => true,
            'htmlOptions' => array(
                'class' => 'form-horizontal well'
            ),
            'clientOptions' => array(
                'validateOnSubmit' => true,
            ),
                ));
        ?>    
        <div class="input-group">   
            <div style="margin-right: 5px;">
                <?php echo $form->textField($model, 'keyword', array('class' => 'form-control', 'placeholder' => 'Your Question.....', 'autocomplete' => 'off')); ?>        
            </div>        
            <span class="input-group-btn">
                <button class="btn btn-primary"><i class="glyphicon glyphicon-search"></i></button>            
            </span>
        </div>    
        <?php $this->endWidget(); ?>
    </div>    
    <!-- /input-group -->
    <div class="row">
        <?php if (isset($_POST['Semantic'])) : ?>    
            <div align="left">
                <h3 style="color: red;">Result</h3>
            </div>
            <table class="table table-hover table-stripped">                
                <tbody>
                    <?php
                    if ($data) {
                        if ($kata_tanya) {
                            echo "jumlah : " . count($data);
                        } else {
                            foreach ($data as $row) {
                                echo '<tr>';
                                echo '<td align="left">' . Semantic::getString($row['?x']) . '</td>';
                                echo '<td align="left">' . Semantic::getString($row['?y']) . '</td>';
                                echo '</tr>';
                            }
                        }
                    } else {
                        echo "<span style='color:red'>Pertanyaan tidak dapat diproses</span>";
                    }
                    ?>
                </tbody>
            </table>            
        <?php endif; ?>
    </div>    
    <?php if ($query != "") : ?>
        <div class="row well" align="left">
            <h3 style="color: red;">Input</h3>
            <?php echo $keyword; ?>
            <h3 style="color: red;">Query</h3>
            <?php echo $query; ?>
        </div>
    <?php endif; ?>
</div>

<?php
return array (
  'template' => 'default',
  'connectionId' => 'db_semantic',
  'tablePrefix' => '',
  'modelPath' => 'application.models',
  'baseClass' => 'CActiveRecord',
  'buildRelations' => '1',
  'commentsAsLabels' => '0',
);

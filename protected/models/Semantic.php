<?php

/**
 * Description of Semantic
 *
 * @author Nur Ichsan
 */
class Semantic extends CFormModel {

    public $keyword;

    public static function sparql($keyword = null) {
        $list_book = array("bahasa_c", "semantic_web", "pengenalan_pola", "tesis1");
        $list_penerbit = array("gramedia", "informatika");
        $list_penulis = array("andi", "anton", "dwi", "agung");
        $list_jurusan = array("fisika", "ilkom");
        //extract individu
        $book = "";
        foreach ($list_book as $val) {
            if (preg_match("/\b$val\b/i", $keyword)) {
                $book = $val;
            }
        }
        $penulis = "";
        foreach ($list_penulis as $val) {
            if (preg_match("/\b$val\b/i", $keyword)) {
                $penulis = $val;
            }
        }
        $penerbit = "";
        foreach ($list_penerbit as $val) {
            if (preg_match("/\b$val\b/i", $keyword)) {
                $penerbit = $val;
            }
        }
        $jurusan = "";
        foreach ($list_jurusan as $val) {
            if (preg_match("/\b$val\b/i", $keyword)) {
                $jurusan = $val;
            }
        }
        $sql = "SELECT ?x ?y ";
        $sql .= "WHERE {" .
                (preg_match("/\bmenulis\b/i", $keyword) ? "?x :menulis " . ($book != "" ? ":$book" : "?y") . "" : "") .
                (preg_match("/\bmenulis\b/i", $keyword) ? ($penulis != "" ? ":$penulis" : "?x") . " :menulis ?y" : "") .
                (preg_match("/\bmenerbitkan\b/i", $keyword) ? "?x :menerbitkan " . ($book != "" ? ":$book" : "?y") . "" : "") .
                (preg_match("/\bmemiliki\b/i", $keyword) ? "?x :memiliki " . ($book != "" ? ":$book" : "?y") . "" : "") .
                (preg_match("/\bditulis\b/i", $keyword) ? "?x :ditulis " . ($penulis != "" ? ":$penulis" : "?y") . "" : "") .
                (preg_match("/\bditerbitkan\b/i", $keyword) ? "?x :diterbitkan " . ($penerbit != "" ? ":$penerbit" : "?y") . "" : "") .
                (preg_match("/\bdimiliki\b/i", $keyword) ? "?x :dimiliki " . ($jurusan != "" ? ":$jurusan" : "?y") . "" : "") .
                //(preg_match("/\btahun terbit\b/i", $keyword) ? "?y :kol_tahun_terbit ?z. FILTER(?z = " . (preg_match("/\b\d{4}\b/", $keyword, $year) ? $year[0] : date(Y)) . ")" : "") .
                "}";

        return $sql;
    }

    public static function getString($word = null) {
        if ($word != NULL || $word != "") {
            $exp1 = explode("#", $word);
            $label = explode('"', $exp1[1]);
            return $label[0];
        } else {
            return $word;
        }
    }

}

?>

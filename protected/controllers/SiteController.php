<?php

class SiteController extends Controller {

    public function actionTest() {
        $subject = "This Island is beautiful";
        $pattern = '/\bisland\b/i';
        if (preg_match($pattern, $subject)) {
            echo 'ada';
        } else {
            echo "hello";
        }
    }

    public function actionIndex() {
        $this->layout = 'main';
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        $model = new Semantic();
        $data = array();
        $query = "";
        $keyword = "";
        $list_kata_tanya = array("berapa", "berapakah");
        $kata_tanya = FALSE;
        if (isset($_POST['Semantic'])) {
//            if (preg_match('/\b\d{4}\b/', $_POST['Semantic']['keyword'], $kata)) {
//                var_dump($kata);
//                exit;
//            }
            Yii::import('application.vendor.*');
            define('RDFAPI_INCLUDE_DIR', 'rdfapi-php/api/');
            require_once('rdfapi-php/api/RdfAPI.php');
            $obj = ModelFactory::getDefaultModel();
            $prefix = 'PREFIX : <http://www.owl-ontologies.com/perpus.owl#> ';
            $obj->load(Yii::getPathOfAlias('webroot') . '/rdf/perpus6.owl');
            $keyword = $_POST['Semantic']['keyword'];
            $sparql = Semantic::sparql($keyword);
            $query = $prefix;
            $query .= $sparql;
            foreach ($list_kata_tanya as $val) {
                if (preg_match("/\b$val\b/i", $keyword)) {
                    $kata_tanya = true;
                }
            }
            $data = $obj->sparqlQuery($query);
        }
        $this->render('index', array(
            'model' => $model,
            'data' => $data,
            'query' => $query,
            'keyword' => $keyword,
            'kata_tanya' => $kata_tanya
        ));
    }

    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

}